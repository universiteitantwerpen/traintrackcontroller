"""
RegEx Tests that run against the output trace for Requirements 3 & 4
Konstantinos Theodorakos
"""

import re
# Read out the complete file to memory
with open("trace.txt", 'r') as f:
    data = f.read()

# First check if there are no violations for segment 1
result1 = re.match("^((([^E].*)|(E [23]))\n)*(E 1\n(.*\n)*G 1\n((([^E].*)|(E [23]))\n)*)*$", data)

# And now check for no violations for segment 2
result2 = re.match("^((([^E].*)|(E [13]))\n)*(E 2\n(.*\n)*G 2\n((([^E].*)|(E [13]))\n)*)*$", data)

# If either does not match, this means that we have a violation!
if result1 is None or result2 is None:
    print("VIOLATION")
else:
    print("CORRECT")

"""
Requirement 3: If a train is on the junction, all traffic lights will remain red until that train has left the junction.
"""
requirement3 = re.findall("E 3\n"       # Train enters the junction
                          "([^X].*\n)*"
                          "G [123]\n"   # Any Green light == VIOLATION
                          "([^X].*\n)*"
                          "X 3", data)  # Train exits the junction

# If there are NO MATCHES, then the Requirement 3 IS NOT violated
if requirement3 == []:
    print("REQUIREMENT 3: CORRECT")
else:
    print("REQUIREMENT 3: VIOLATION")
    print requirement3

"""
Requirement 4: If two trains are waiting to enter the junction at the same time, permission will be granted
in order of arrival (i.e., the first train to arrive will get a green light, and the second one has to wait).
"""
requirement4 = re.findall("E ([12])\n" # 1st train IN (Group 1)
                          "([^X].*\n)*"
                          "E ([^\\1]|[^3])\n" # 2nd train IN (Group 3)
                          "([^X].*\n)*"
                          "X \\3\n" # 2nd train OUT (from Group 3) aka VIOLATION
                                    # (train from Group 1 should exit first)
                          "([^X].*\n)*"
                          "X \\1\n" # 1st train OUT (from Group 1)
                          , data)

# If there are NO MATCHES, then the Requirement 4 IS NOT violated
if requirement4 == []:
    print("REQUIREMENT 4: CORRECT")
else:
    print("REQUIREMENT 4: VIOLATION")
    print requirement4