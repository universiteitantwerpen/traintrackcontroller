"""
FSA Tests that run against the output trace for Requirements 3 & 4
Konstantinos Theodorakos
"""
import charstream
import scanner

textFile = open("trace.txt", "r")
allLines = textFile.read()
textFile.close()

"""
Requirement 3
"""
inp = allLines
str=charstream.CharacterStream(inp)
sc=scanner.Requirement3Scanner(str)
success=sc.scan()
print "\n=== Requirement 3 ===\n"
if success:
    print ">> recognized "+sc.__str__()
    print ">> rolling back"
    str.rollback()
    print str
    print ""
    print("REQUIREMENT 3: VIOLATION")
else:
    print ">> rejected"
    print ">> committing"
    str.commit()
    print("REQUIREMENT 3: CORRECT")

"""
Requirement 4
"""
inp = allLines
str=charstream.CharacterStream(inp)
sc=scanner.Requirement4Scanner(str)
success=sc.scan()
print "\n=== Requirement 4 ===\n"
if success:
    print ">> recognized "+sc.__str__()
    print ">> rolling back"
    str.rollback()
    print str
    print ""
    print("REQUIREMENT 4: VIOLATION")
else:
    print ">> rejected"
    print ">> committing"
    str.commit()
    print("REQUIREMENT 4: CORRECT")